const { markdown, danger, warn, fail, message } = require('danger');

const BASE_PROJECT_ID = '55395528';

const warnWithCommitId = (message, commitId) => warn(`${commitId}: ${message}`);

(async function () {

    if (danger.gitlab) {
        if (danger.gitlab.mr.target_project_id != BASE_PROJECT_ID) {
            return;
        }

        const commitFiles = [
            ...danger.git.modified_files,
            ...danger.git.created_files,
            ...danger.git.deleted_files
        ]

        if (commitFiles.length === 0) {
            fail('No files changed. Please consider adding some changes.')
        }

        const hasChangesLog = commitFiles.some(file => file.includes('CHANGELOG.md'))

        if (!hasChangesLog) {
            message('### Changelog Missing:\nNo CHANGELOG.md file found. Please consider adding one.\nP.S if these The merge request does not require a CHANGELOG.md file, please ignore this message.')
        }


        if (danger.gitlab.mr.title.trim().length === 0 || danger.gitlab.mr.title.trim().length > 100) {
            fail('The merge request title should be between 1 and 100 characters')
        }

        if (!danger.gitlab.mr.title[0].match(/[A-Z]/)) {
            warn('The merge request title should start with uppercase')
        }

        if (danger.gitlab.mr.description.trim().length === 0) {
            warn('The merge request description should not be empty')
        }


        danger.git.commits.forEach(commit => {
            const commitMessage = commit.message.trim()
            const commitId = commit.sha

            if (commitMessage.length === 0) {
                warnWithCommitId('The commit message should not be empty', commitId)
            }

            if (!commitMessage[0].match(/[A-Z]/)) {
                warnWithCommitId('The commit message should start with uppercase', commitId)
            }

            if (commitMessage.split(' ').length < 3) {
                warnWithCommitId('The commit message should have atleast 3 words', commitId)
            }

            if (commitMessage.length > 72) {
                warnWithCommitId('The commit message should be less than 100 characters', commitId)
            }

        })

        if (!danger.gitlab.mr.milestone) {
            warn('This merge request does not have a milestone')
        }

        const scalaFiles = commitFiles.filter(file => file.endsWith('.scala'))

        for (file of scalaFiles) {
            const fileContent = await danger.gitlab.utils.fileContents(file)
            if (fileContent.includes('println')) {
                fail(`The file \`${file}\` contains a println statement. Please remove it.`)
            }

            const testFile = file.replace('.scala', 'Spec.scala').replace('src/main', 'src/test')
            if (!commitFiles.includes(testFile)) {
                warn(`The file \`${file}\` was modified but the corresponding test file \`${testFile}\` was not modified.`)
            }
        }

        for (const file of commitFiles.filter(file => file.includes("gradle"))) {
            warn(`A gradle file \`${file}\` was modified. Please make sure this change is necessary.\n We do not recommend changing gradle files unless it is necessary.`)
        }

        // check for significant file size changes
        for (const file of commitFiles) {
            const fileContent = await danger.gitlab.utils.fileContents(file);
            // Use the length of the content as a rough approximation of file size
            if (fileContent.length > 1024 * 1024) {
                warn(`The file \`${file}\` is larger than 1MB. Please check if such a large file is necessary.`);
            }
        }

        markdown("## ✅ Danger Report\n\nDanger has finished running. Please check the comments above for any warnings or errors. Thanks for your contribution! ❤️")
    }
})()
